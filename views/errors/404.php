<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>404</title>
	</head>
	<style type="text/css">
		code {
			font-size:5em;
			color:rgba(95%, 43%, 9%,1);
			text-align:center;
			display:block;
			position:relative;
			top:100px;
			text-shadow: 2px 2px 6px rgba(33%, 33%, 33%, 1);
			-webkit-box-reflect:below 5px -webkit-gradient(linear, left top, left bottom, from(transparent), color-stop(0.5, transparent), to(white));
		}
		code span {color:rgba(13%, 13%, 13%, 1); }
		body {background:#FFF; }
	</style>
	<body>
		<code> &lt;?php<span> 404 NOT FOUND </span>?&gt; </code>
	</body>
</html>
