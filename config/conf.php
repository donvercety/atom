<?php

# # # # # # # # # # # # # # # # # # # # # # # # #
#              Globals Config Array             #
# # # # # # # # # # # # # # # # # # # # # # # # #
#
#   MySQL Connection Config:
#
#   Specifying the needed information for database login,
#   leave blank if no database connection required.
#

$GLOBALS['config']['mysql']['host']     = 'localhost';
$GLOBALS['config']['mysql']['username'] = 'test';
$GLOBALS['config']['mysql']['password'] = 'qwerty';
$GLOBALS['config']['mysql']['db']       = 'test';

#
#   Models & Views folder Config:
#
#   Specify location and name of models & views folder.
#   Defailt value for views: './views/';
#   Defailt value for models: './models/';
#

$GLOBALS['config']['view_path']     = './views/';
$GLOBALS['config']['model_path']    = './models/';

#
#   Public HTML Config:
#
#   Specify location and name of public_html folder.
#   Defailt value: 'public_html/';
#

$GLOBALS['config']['public_html'] = 'public_html/';

#
#	Default errors folder.
#

$GLOBALS['config']['errors']['404'] = '/errors/404';

#
#   Loading Session Helper:
#
#   TRUE    - ON
#   FALSE   - OFF
#

$GLOBALS['config']['session']['state']  = FALSE;

#
#   Working with sessions and cookies:
#   Can be used with Token Class and sample model User Class.
#

$GLOBALS['config']['remember']['cookie_name']   = 'hash';
$GLOBALS['config']['remember']['cookie_expiry'] = 604800;
$GLOBALS['config']['session']['session_name']   = 'user';
$GLOBALS['config']['session']['token_name']     = 'token';

#
#
# # # # # # # # # # # # # # # # # # # # # # # # #
#           Globals Config Array End            #
# # # # # # # # # # # # # # # # # # # # # # # # #
