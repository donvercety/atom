<?php
# # # # # # # # # # # # # # # # # # # # # # # # #
#                 Routing File                  #
# # # # # # # # # # # # # # # # # # # # # # # # #
#
#   Required:   default controller
#   
#   If functions will be used instead of controllers
#   comment the "default" lines from the config file
#   and use function calls as explained below.
#

$GLOBALS['config']['default_route']      = 's';
$GLOBALS['config']['default_controller'] = 'welcome';

#   =============================
#   Add additional routing below:
#   =============================
#   
#   first  parametur = route
#   second parametur = controller
#

#	$route->add('/about', 'About');


#   ===========================================
#   Use $route for directly calling a function:
#   =========================================== 
#
#       $route->add('/', function() {
#         // code
#       }
#
#   ===============================
#   Use the parameters from the URI
#   ===============================
#
#   Add $params = array() to the function or some parametur
#   to the controller's method to have access to the values
#   provided in the URI after the controller.
#   
#   URI example: http://localhost/abaout/index/val_1/val_2
#
#   index is the default method
#
#   Using function:
#   ===============
#
#       $route->add('/about', function($params = array()) {
#         // code
#       }
#
#
#   Using Controllers:
#   ==================
#
#       class About {
#       
#             public function index($values) {
#                 // code
#             }
#       }
#
# # # # # # # # # # # # # # # # # # # # # # # # #
#                Routing File End               #
# # # # # # # # # # # # # # # # # # # # # # # # #
