<?php
# # # # # # # # # # # # # # # # # # # # # # # # #
#              Initialization File              #
# # # # # # # # # # # # # # # # # # # # # # # # #

#
#	Making DIRECTORY_SEPARATOR Shorter :) 
#

defined('DS') ? null : define('DS', DIRECTORY_SEPARATOR);

# 
#	Loading Global Config Array 
#

require_once './config/conf.php';

#
#	=====================
#	Classes & Controllers
#	=====================
#
#	Autoload App. Classes:
#

	function autoload_classes($class_name) {
		$file = './app/' . $class_name . '.php';
		if (file_exists($file)) {
			require_once($file);
		}
	}

#
#	Autoload Controllers:
#

	function autoload_controllers($class_name) {
		$file = './controllers/' . $class_name . '.php';
		if (file_exists($file)) {
			require_once($file);
		}
	}

	spl_autoload_register('autoload_classes');
	spl_autoload_register('autoload_controllers');

#
#	================
#	Helper functions
#	================
#	
#

defined('SITE_URL') ? null :
                define('SITE_URL', HTTP_PATH);
	
#
#	public_url() for links in the public_html folder
#

	function public_url($path = '') {
		$url = SITE_URL . Config::get('public_html') . $path;
		return $url;
	}
	
#	
#	site_url() for links regarding routing
#

	function site_url($path = '') {
		$url = SITE_URL . $path;
		return $url;
	}

#
#	active() checks if link is active
#
	
	function active($state, $active) {
		if ($state == $active) {
			echo 'active';
		} else {
			echo "";
		}
	}

# 
# 	================
#	Starting Session 
#	================
#	

	if(Config::get('session/state') == 1) {
		session_start();
	}

# 
# 	===================
#	Connect to Database 
#	===================
#

	DB::settings([
		"host" => Config::get("mysql/host"),
		"db"   => Config::get("mysql/db"),
		"user" => Config::get("mysql/username"),
		"pass" => Config::get("mysql/password")
	]);

# # # # # # # # # # # # # # # # # # # # # # # # #
#           Initialization File End             #
# # # # # # # # # # # # # # # # # # # # # # # # #
