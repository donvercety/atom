<?php
# # # # # # # # # # # # # # # # # # # # # # # # #
#                Main Index File                #
#                 DO NOT MODIFY                 #
# # # # # # # # # # # # # # # # # # # # # # # # #

#
#   HTTP_PATH for front end logic
#

defined('HTTP_PATH') ? null :
                define('HTTP_PATH', "http://$_SERVER[SERVER_NAME]" . dirname($_SERVER['PHP_SELF']));

#
#   ROOT_PATH for php logic
#

defined('ROOT_PATH') ? null : define('ROOT_PATH', dirname(__FILE__));

#
#   Inserting the required resources
#   for proper application functionality.
#

require_once './config/init.php';

#
#   Starting - Routing System
#

$route = new Route();

#
#   Fetrching users routes:
#   User routes are declared in route.php
#   located in the config folder.
#

require_once './config/route.php';

#
#   Default Route:
#   
#   Works for both: http://web-address/
#                   http://web-address/default_route
#

$route->add('/' ,   Config::get('default_controller'));

$route->add('/' .   Config::get('default_route'),
                    Config::get('default_controller'));

#
#   Executing Routing
#

$route->submit();

# # # # # # # # # # # # # # # # # # # # # # # # #
#             Main Index File End               #
# # # # # # # # # # # # # # # # # # # # # # # # #
