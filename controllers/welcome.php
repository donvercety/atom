<?php

/**
 * Default Welcome Controller
 */
class Welcome {

	/**
	 * Default Method
	 */
	public function index() {
		Load::view('welcome');
	}
}