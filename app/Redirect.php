<?php

/**
 * A simple redirecting class.
 *
 * @author Tommy Vercety
 */
class Redirect {

    /**
     * Redirect to method. Can be used to make error handling like 404.
     * @param  string $location
     */
    public static function to($location = NULL) {
        if ($location) {
            if (is_numeric($location)) {
                switch ($location) {
                    case 404:
                        header('HTTP/1.0 404 Not Found');
                        include 'views/errors/404.php';
                        exit();
                        break;
                }
            }
            header('Location: ' . $location);
            exit();
        }
    }

}
