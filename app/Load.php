<?php
/**
 * Loads views and models
 *
 * @uses Config Class
 * @author Tommy Vercety
 */
class Load {
 
	/**
	 *	Loads Views.
	 *
	 *	Directly loading the selected view,
	 *  if second parameter is passed, the array
	 *  is passed to the view and its keys
	 *  are converted to variables.
	 *	
	 *	@param array $data
	 *	@param string $template
	 */
	public static function view($template, $data = array()) {
		$path = Config::get('view_path') . $template . '.php';
		if (file_exists($path)) {
			extract($data);
			require($path);
		}
	}
	
	/**
	 * 	Loads Models
	 *  
	 *  Models must get DB instance in their constructor.
	 *  This function only loads the desired model, afterwards
	 *  that model must be instantiated.
	 * 
	 *  @param  string $model
	 */
	public static function model($model) {
		$path = Config::get('model_path') . $model . '.php';
		if (file_exists($path)) {
			require $path;
		}
        
    }

 }