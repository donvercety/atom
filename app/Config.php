<?php

/**
 * Class for getting configuration form
 * a $GLOBALS array, the file is usually
 * called conf.php and it can be found in
 * the config folder.
 *
 * @author Tommy Vercety
 */
class Config {

    /**
     * Getting values from the config array.
     * @param  string $path
     * @return mixed
     */
    public static function get($path = NULL) {
        if ($path) {
            $config = $GLOBALS['config'];
            $path = explode('/', $path);

            foreach ($path as $bit) {
                if (isset($config[$bit])) {
                    $config = $config[$bit];
                }
            }

            return $config;
        }

        return FALSE;
    }

}
