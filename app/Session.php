<?php

/**
 * Simple Session Wrapper v1.0
 *
 * @author Tommy Vercety
 */
class Session {

    /**
     * Indicates if session is started.
     * @var boolean
     */
    private static $_sessionStarted = FALSE;

    /**
     * Start the Session.
     */
    public static function start()
    {
        if (self::$_sessionStarted == FALSE)
        {
            session_start();
            self::$_sessionStarted = TRUE;
        }
    }

    /**
     * Setting a Session Variable.
     * @param mixed $key   [the $key for $_SESSION]
     * @param mixed $value [the $value for $_SESSION]
     */
    public static function set($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    /**
     * Getting a Session Variable based on a given key.
     * @param  mixed $key
     * @param  mixed $secondKey
     * @return mixed
     */
    public static function get($key, $secondKey = FALSE)
    {
        if ($secondKey == TRUE)
        {
            if (isset($_SESSION[$key][$secondKey]))
                return $_SESSION[$key][$secondKey];
        }
        else
        {
            if (isset($_SESSION[$key]))
                return $_SESSION[$key];
        }

        return FALSE;
    }

    /**
     * Checking if a Session Variable is set.
     * @param  mixed $key
     * @return boolean
     */
    public static function exists($key)
    {
        return (isset($_SESSION[$key])) ? TRUE : FALSE;
    }

    /**
     * Deleating a Session Variable based on a given key.
     * @param  mixed $key
     * @return void
     */
    public static function delete($key)
    {
        if (self::exists($key))
        {
            unset($_SESSION[$key]);
        }
    }

    /**
     * Prints the content of the Session.
     * Use only for debugging, in development mode.
     * @return void
     */
    public static function display()
    {
        echo '<pre>', print_r($_SESSION, TRUE), '</pre>';
    }

    /**
     * Used for sending a message to the user.
     * 
     * Use is with a key & string to set the message,
     * then the second time, call it only with
     * a key to  display the message to the user
     * and then it will be automaticcly removed.
     * 
     * @param  mixed $key
     * @param  string $string
     * @return mixed
     */
    public static function flash($key, $string = '')
    {
        if (self::exists($key))
        {
            $session = self::get($key);
            self::delete($key);
            return $session;
        }
        else
        {
            self::set($key, $string);
        }
        return '';
    }

    /**
     * Destroying the Session and all of its content.
     * @return void
     */
    public static function destroy()
    {
        if (self::$_sessionStarted == TRUE)
        {
            session_unset();    // Might need work!
            session_destroy();
        }
    }

}
